# jing - Bing's photo of today on your console

Tiny program that shows the [Bing](https://www.bing.com/images/) image of the day on your ANSI terminal, using unicode and truecolor.

![screenshot](screenshot.jpg)
