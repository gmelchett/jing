package main

import (
	"fmt"

	"log"
	"net/http"
	"strings"

	"bitbucket.org/gmelchett/jonsi"
	"bitbucket.org/gmelchett/textcolor"
	"github.com/mmcdole/gofeed"
)

func main() {

	fp := gofeed.NewParser()
	bing, err := fp.ParseURL("https://www.bing.com/HPImageArchive.aspx?format=rss&idx=0&n=1&mkt=en-US")
	if err != nil {
		log.Fatalf("Failed to download today's feed from bing.com. Error: %s\n", err)
	}
	resp, err := http.Get("https://www.bing.com/" + bing.Items[0].Link)
	if err != nil {
		log.Fatalf("Failed to download today's photo from bing.com. Error: %s\n", err)
	}

	defer resp.Body.Close()

	if s, _, cols, err := jonsi.NewFromReader(resp.Body, jonsi.KeepAspectRatio, jonsi.FullWidth, jonsi.QuadrantBlock); err == nil {
		fmt.Println(s)
		t := bing.Items[0].Title
		if strings.Contains(t, "(©") {
			t = t[:strings.Index(t, "(©")]
		}
		if cols > len(t) {
			t = strings.Repeat(" ", (2*cols-len(t))/2) + t
		}
		fmt.Println(textcolor.BoldOn + t + textcolor.Reset + "\n")
	} else {
		log.Fatalf("Failed to decode image. Error: %s\n", err)
	}
}
